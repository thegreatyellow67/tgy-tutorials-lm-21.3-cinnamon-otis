- Per l'applet Cambio area di lavoro imposta i nomi delle aree di lavoro:
4 aree di lavoro: web, chat, term, media

- Visibilità del pannello:
Pannello a scomparsa automatica: nascondi il pannello in modo intelligente

- Impostazioni del pannello:
	Altezza del pannello: 32
	Posizione: verticale sx
	Zona superiore:
	Dimensione dei caratteri: default
	Dimensione delle icone colorate: 24px
	Dimensione delle icone simboliche: 24px
	Zona centrale:
	Dimensione dei caratteri: default
	Dimensione delle icone colorate: 22px
	Dimensione delle icone simboliche: 22px
	Zona inferiore:
	Dimensione dei caratteri: 6.5pt (per calendario)
	Dimensione delle icone colorate: 16px
	Dimensione delle icone simboliche: 16px

- Scaricare le seguenti applet:
	- Meteo
	- Radio++
	- GPaste Reloaded

- Estensioni:
	- Pannelli trasparenti
	- Opacizza finestre
	- Horizontal OSD

- Applet Calendario:
Utilizza un formato della data personalizzato: ATTIVO
Formato della data: %H:%M:%S
Formato della data per il tooltip: %A %d %B, %H:%M

- Applet Radio++:
Al primo click far installare mpv

NOTA BENE: ripristinare le impostazioni che si trovano nei files:
~/Scaricati/lm-21.3-cinnamon-otis/applets-confs
~/Scaricati/lm-21.3-cinnamon-otis/extensions-confs

- Ordine delle applets nel pannello:
Zona superiore:
1. Menù (menu@cinnamon.org)
2. Separatore
3. Elenco finestre raggruppate (grouped-window-list) con icone questa volta bloccate al pannello uso lanciatore applicazioni
icone dell'applet: Impostazioni di sistema, Terminale, File, Monitor di sistema, Calcolatrice, Gestore applicazioni, Firefox Web Browser, Email Mozilla Thunderbird, Editor di testo, LibreOffice, Pix, Lettore Multimediale VLC, Calendario

Zona centrale:
Niente
Zona inferiore:
4. Meteo (weather)
5. Radio++ (radio@driglu4it)
6. GPaste Reloaded
7. Update Manager (no applet, solo icona systray della app)
8. Unità rimovibili (removable-drives)
9. Gestione reti (network)
10. Audio (sound)
11. Separatore
12. Calendario (calendar)

Togli l'applet Mostra il desktop

