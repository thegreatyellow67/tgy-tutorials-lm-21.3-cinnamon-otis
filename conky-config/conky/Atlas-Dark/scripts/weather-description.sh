#!/bin/bash

#weather_desc=`cat ~/.cache/weather.json | jq -r '.weather[0].description' | sed "s|\<.|\U&|g"`
weather_desc=`cat ~/.cache/weather.json | jq -r '.weather[0].description'`

case ${weather_desc} in

  ## Group: Thunderstorm ##
  "thunderstorm with light rain")
    weather_desc="temporale con pioggia leggera"
    ;;

  "thunderstorm with rain")
    weather_desc="temporale con pioggia"
    ;;

  "thunderstorm with heavy rain")
    weather_desc="temporale con forti piogge"
    ;;

  "light thunderstorm")
    weather_desc="temporale leggero"
    ;;

  "thunderstorm")
    weather_desc="temporale"
    ;;

  "heavy thunderstorm")
    weather_desc="temporale forte"
    ;;

  "ragged thunderstorm")
    weather_desc="temporale irregolare"
    ;;

  "thunderstorm with light drizzle")
    weather_desc="temporale con leggera pioggerellina"
    ;;

  "thunderstorm with drizzle")
    weather_desc="temporale con pioviggine"
    ;;

  "thunderstorm with heavy drizzle")
    weather_desc="temporale con forte pioviggine"
    ;;

    ## Group: Drizzle
  "light intensity drizzle")
    weather_desc="pioviggine di leggera intensità"
    ;;

  "drizzle")
    weather_desc="pioviggine"
    ;;

  "heavy intensity drizzle")
    weather_desc="pioviggine di forte intensità"
    ;;

  "light intensity drizzle rain")
    weather_desc="pioviggine di leggera intensità"
    ;;

  "drizzle rain")
    weather_desc="pioggia piovigginosa"
    ;;

  "heavy intensity drizzle rain")
    weather_desc="pioggia piovigginosa di forte intensità"
    ;;

  "shower rain and drizzle")
    weather_desc="rovesci di pioggia e pioviggine"
    ;;

  "heavy shower rain and drizzle")
    weather_desc="forte pioggia e pioviggine"
    ;;

  "shower drizzle")
    weather_desc="rovesci di pioviggine"
    ;;

  ## Group: Rain ##
  "light rain")
    weather_desc="pioggia leggera"
    ;;

  "moderate rain")
    weather_desc="pioggia moderata"
    ;;

  "heavy intensity rain")
    weather_desc="pioggia di forte intensità"
    ;;

  "very heavy rain")
    weather_desc="pioggia molto forte"
    ;;

  "extreme rain")
    weather_desc="pioggia estrema"
    ;;

  "freezing rain")
    weather_desc="grandine"
    ;;

  "light intensity shower rain")
    weather_desc="pioggia di leggera intensità"
    ;;

  "shower rain")
    weather_desc="rovesci di pioggia"
    ;;

  "heavy intensity shower rain")
    weather_desc="rovesci di pioggia di forte intensità"
    ;;

  "ragged shower rain")
    weather_desc="rovesci di pioggia irregolari"
    ;;

  ## Group: Snow ##
  "light snow")
    weather_desc="neve leggera"
    ;;

  "snow")
    weather_desc="neve"
    ;;

  "heavy snow")
    weather_desc="forte nevicata"
    ;;

  "sleet")
    weather_desc="nevischio"
    ;;

  "light shower sleet")
    weather_desc="nevischio leggero"
    ;;

  "shower sleet")
    weather_desc="rovescio di nevischio"
    ;;

  "light rain and snow")
    weather_desc="pioggia leggera e neve"
    ;;

  "rain and snow")
    weather_desc="pioggia mista a neve"
    ;;

  "light shower snow")
    weather_desc="leggero rovescio di neve"
    ;;

  "shower snow")
    weather_desc="rovescio di neve"
    ;;

  "heavy shower snow")
    weather_desc="intenso rovescio di neve"
    ;;

  ## Group: Atmosphere ##
  "mist")
    weather_desc="nebbia"
    ;;

  "smoke")
    weather_desc="fumo"
    ;;

  "haze")
    weather_desc="foschia"
    ;;

  "sand/dust whirls")
    weather_desc="vortici di sabbia/polvere"
    ;;

  "fog")
    weather_desc="nebbia"
    ;;

  "sand")
    weather_desc="sabbia"
    ;;

  "dust")
    weather_desc="polvere"
    ;;

  "volcanic ash")
    weather_desc="cenere vulcanica"
    ;;

  "squalls")
    weather_desc="burrasche"
    ;;

  "tornado")
    weather_desc="tornado"
    ;;

  ## Group: Clear ##
  "clear sky")
    weather_desc="cielo sereno"
    ;;

  ## Group: Clouds ##
  "few clouds")
    weather_desc="poco nuvoloso"
    ;;

  "scattered clouds")
    weather_desc="nubi sparse"
    ;;

  "broken clouds")
    weather_desc="nubi interrotte"
    ;;

  "overcast clouds")
    weather_desc="coperto"
    ;;

esac

echo ${weather_desc} | sed "s|\<.|\U&|g"
